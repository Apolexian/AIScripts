import cv2
import numpy as np
import os
import tflearn
from random import shuffle
from tqdm import tqdm
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.estimator import regression


trainDir = "train/"
testDir = "test/"
imgSize = 50
lr = 0.01

modelName = "catvsdogs_{}_{}".format(lr,"conv")

def labelConvert(img):
    wordLabel = img.split('.')[-3]
    if wordLabel == 'cat': return [1,0]
    elif wordLabel == 'dog': return [0,1]


def processTrain():
    train = []
    for img in tqdm(os.listdir(trainDir)):
        label = labelConvert(img)
        path = os.path.join(trainDir,img)
        img = cv2.imread(path,cv2.IMREAD_GRAYSCALE)
        img = cv2.resize(img,(imgSize,imgSize))
        train.append([np.array(img),np.array(label)])

    shuffle(train)
    np.save('trainData.npy', train)
    return train


def processTest():
    test = []
    for img in tqdm(os.listdir(testDir)):
        path = os.path.join(testDir,img)
        img_num = img.split('.')[0]
        img = cv2.imread(path,cv2.IMREAD_GRAYSCALE)
        img = cv2.resize(img, (imgSize,imgSize))
        test.append([np.array(img), img_num])

    shuffle(testing_data)
    np.save('testData.npy', test)
    return test

trainData = processTrain()
#trainData = np.load('trainData.npy')
network = input_data(shape=[None, imgSize, imgSize, 1], name='input')
network = conv_2d(network, 32, 5, activation='relu')
network = max_pool_2d(network, 5)
network = conv_2d(network, 64, 5, activation='relu')
network = max_pool_2d(network, 5)
network = conv_2d(network, 128, 5, activation='relu')
network = max_pool_2d(network, 5)
network = conv_2d(network, 64, 5, activation='relu')
network = max_pool_2d(network, 5)
network = conv_2d(network, 32, 5, activation='relu')
network = max_pool_2d(network, 5)
network = fully_connected(network, 1024, activation='relu')
network = dropout(network, 0.8)
network = fully_connected(network, 2, activation='softmax')
network = regression(network, optimizer='adam', learning_rate=lr, loss='categorical_crossentropy', name='targets')

model = tflearn.DNN(network, tensorboard_dir='log')

if os.path.exists('{}.meta'.format(modelName)):
    model.load(modelName)
    print('model loaded!')

train = trainData[:-500]
test = trainData[-500:]

X = np.array([i[0] for i in train]).reshape(-1,imgSize,imgSize,1)
Y = [i[1] for i in train]

testX = np.array([i[0] for i in test]).reshape(-1,imgSize,imgSize,1)
testY = [i[1] for i in test]

if os.path.exists('{}.meta'.format(modelName)):
    model.load(modelName)
    print('Loaded!')

model.fit({'input': X}, {'targets': Y}, n_epoch=10, validation_set=(
                        {'input': testX}, {'targets': testY}),
                        snapshot_step=500, show_metric=True, run_id=modelName)

model.save(modelName)
