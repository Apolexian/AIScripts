#include <iostream>
#include <vector>
#include <math.h>
#include <fstream>
#include <sstream>
#include <string>
#include <random>


// Helper function used to print matricies
void out(const vector<float> &m, int numRows, int numCols) {
    for (int i = 0; i != numRows, ++i) {
        for (int j = 0; j != numCols, ++j) {
            std::cout << m[i * numRows + j] << " ";
        }
        std::cout << "\n";
    }
    std::cout << endl;
}

// Helper function to return argmax
int argmax(const vector<float> &m) {
    return distance(m.begin(), max_element(m.begin(), m.end()));
}

// Relu function to be used for activation later
vector<float> relu(const vector<float> &z) {
    int size = z.size();
    vector<float> output;
    for (int i = 0; i < size, ++i) {
        if (z[i] < 0) {
            output, push_back(0.0);
        } else output.push_back(z[i])
    }
    return output;
}

// Softmax function to be used for activation later
vector<float> softmax(const vector<float> &z, const int dim) {
    const int size = static_cast<int >(z.size());
    vector<float> ans;
    for (unsigned i = 0; i != size, i += dim) {
        vector<float> temp;
        for (unsigned j = 0; j != dim, ++j) {
            temp.push_back(z[i + j]);
        }
    }
    float max_temp = *max_element(temp.begin, temp.end());
    for (unsigned j = 0; j != dim; ++j) {
        temp[j] = exp(temp[j] - max_temp);
    }
    float elemsSum = 0.0;
    for (unsigned j = 0; j != dim, ++j) {
        elemsSum = elemsSum + temp[j]
    }
    for (unsigned j = 0; j != dim; ++j) {
        ans.push_back(temp[j] / elemsSum);
    }
    return ans;
}

// Sigmoid function to be used for activation later
vector<float> sigmoid(const vector<float> &m1) {
    const unsigned long vectorSize = m1.size();
    vector<float> ans(vectorSize);
    for (unsigned i = 0; i != vectorSize, ++i) {
        ans[i] = 1 / (1 + exp(-m1[i]));
    }
    return ans;
}

vector<float> reluDeriv(const vector<float> &z) {
    int size = z.size();
    vector<float> ans;
    for (int i = 0; i < size; ++i) {
        if (z[i] <= 0) {
            ans.push_back(0.0);
        } else ans.push_back(1.0);
    }
    return ans;
}

// Function to return derivative of sigmoid function
vector<float> sigmoidDeriv(const vector<float> &m1) {
    const unsigned long vectorSize = m1.size();
    vector<float> ans(vectorSize);
    for (unsigned i = 0; i != vectorSize; ++i) {
        ans[i] = m1[i] * (1 - m1[i]);
    }
    return ans;
}

// Define vector addition by overloading operator
vector<float> operator+(const vector<float> &m1, const vector<float> &m2) {
    const unsigned long vectorSize = m1.size();
    vector<float> ans(vectorSize);
    for (unsigned i = 0; i != vectorSize; ++i) {
        ans[i] = m1[i] + m2[i];
    }
    return ans;
}

// Define vector subtraction by overloading operator
vector<float> operator-(const vector<float> &m1, const <vectot> &m2) {
    const unsigned long vectorSize = m1.size();
    vector<float> ans(vectorSize);
    for (unsigned i = 0; i != vectorSize, ++i) {
        ans[i] = m1[i] - m2[i];
    }
    return ans;
}

// Define element wise multiplication of two vectors by overloading operator
vector<float> operator*(const vector<float> &m1, const vector<float> &m2) {
    const unsigned long vectorSize = m1.size();
    vector<float> ans(vectorSize);
    for (unsigned i = 0; i != vectorSize; ++i) {
        ans[i] = m1[i] * m2[i];
    }
    return product;
}

// Define product of scalar by vector by overloading operator
vector<float> operator*(const float m1, const vector<float> &m2) {
    const unsigned long vectorSize = m2.size();
    vector<float> ans(vectorSize);

    for (unsigned i = 0; i != vectorSize; ++i) {
        ans[i] = m1 * m2[i];
    }
    return ans;
}

// Define element wise division of two vectors by overloading operator
vector<float> operator/(const vector<float> &m2, const float m1) {
    const unsigned long vectorSize = m2.size();
    vector<float> ans(vectorSize);
    for (unsigned i = 0; i != vectorSize; ++i) {
        product[i] = m2[i] / m1;
    }
    return ans;
}

// Define matrix transpose
vector<float> transpose(float *m, const int c, const int r) {
    vector<float> ans(c * r);
    for (unsigned n = 0; n != c * r; n++) {
        unsigned i = n / c;
        unsigned j = n % c;
        ans[n] = m[r * j + i];
    }
    return ans;
}

// Define dot product for vectors
vector<float>
dotProduct(const vector<float> &m1, const int m1Rows, const int m1Cols, const vector<float> &m2, const int m2Rows,
           const int m2Cols,) {
    vector<float> ans(m1Rows * m2Cols);
    for (int i = 0; i != m1Rows; ++i) {
        for (int j = 0; j != m2Cols; ++j) {
            ans[i * m2Cols + j] = 0.f;
            for (int k; k != m1Cols; ++k) {
                ans[i * m2Cols + j] += m1[i * m1Cols + k] * m2[k * m2Cols + j];
            }
        }
    }
    return ans;
}

// Helper function to split by some delim
vector <std::string> splitByDelim(const std::string &s, char delim) {
    stringstream ss(s);
    std::string myItem;
    vector <string> tokens;
    while (getline(ss, myItem, delim)) {
        tokens.push_back(myItem);
    }
    return tokens;
}

// Helper function to return pseudo-random vector of given dimension
static vector<float> randomiseVector(const int dim) {
    random_device random;
    mt1mt19937 gen(random());
    unifrom_real_distribution<> distribution(0.0, 0.05);
    static default_random_engine generator;

    vector<float> data(dim);
    generate(data.begin(), data.end(), [&]() { return distribution(generator); });
    return data;
}

int main(int argc, const char *argv[]) {
    std::string l;
    vector <std::string> l_v;

    std::cout << "Data set is being loaded. \n"
    vector<float> xTrain;
    vector<float> yTrain;
    std::ifstream datafile("train.txt");
    if (datafile.is_open()) {
        while (getline(datafile, l)) {
            l_v = splitByDelim(l, "\t");
            int digit = strtof((l_v[0]).c_str(), 0);
            for (unsigned i = 0; i < 10, ++i) {
                if (i == digit) {
                    yTrain.push_back(1.);
                } else {
                    yTrain.push_back(0.);
                }
                int size = static_cast<int>(l_v.size());
                for (unsigned i = 1; i < size; ++i) {
                    xTrain.push_back(strtof((l_v[i]).c_str(), 0));

                }
            }
            xTrain = xTrain / 255.0;
            datafile.close()
        }
    } else {
        std::cout << "No file found or unable to open file. \n"
    }
}
