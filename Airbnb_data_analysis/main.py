import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


# create the data frame and remove any unnecessary columns
# create a new csv of this data frame to operate on
# to not damage original data accidentally
def create_df():
    data = pd.read_csv("AB_NYC_2019.csv")
    cols = ['id', 'name', 'host_id', 'host_name',
            'last_review', 'reviews_per_month',
            'calculated_host_listings_count', 'availability_365']
    data.drop(cols, axis=1, inplace=True)
    data = data.sort_values('neighbourhood_group').reset_index(drop=True)
    data.isnull().sum()
    data.to_csv(r'ABNYC_data.csv')
    print('Data frame created')
    return data


# Plot the longitude and latitude of properties on a map to visualise data set
def plot_coords(df):
    img = plt.imread("img.jpg")
    plt.scatter(x=df['longitude'], y=df['latitude'], s=2)
    implot = plt.imshow(img, extent=[min(df['longitude']),
                                     max(df['longitude']),
                                     min(df['latitude']),
                                     max(df['latitude'])])
    plt.show()


# Plot the distribution of room types as a pir chart
def pie_rooms(df):
    room_df = df.filter(['room_type'], axis=1)
    room_df = room_df.groupby('room_type').size().reset_index(name='Count')
    explode = (0.1, 0.1, 0.1)
    plt.pie(room_df['Count'], shadow=True, autopct='%1.1f%%',
            labels=room_df['room_type'],
            startangle=130, explode=explode)
    plt.title('Room type distribution', bbox={'facecolor': '0.8', 'pad': 5})
    plt.axis('equal')
    plt.tight_layout()
    plt.show()


# Plot the distribution of bookings by area
def pie_area(df):
    area_df = df.filter(['neighbourhood_group'], axis=1)
    area_df = area_df.groupby('neighbourhood_group').size().reset_index(
                                                            name='Count')
    explode = (0.1, 0.1, 0.1, 0.1, 0.1)
    plt.pie(area_df['Count'], shadow=False, autopct='%1.1f%%',
            labels=area_df['neighbourhood_group'],
            startangle=150, explode=explode)
    plt.title('Bookings by area', bbox={'facecolor': '0.8', 'pad': 5})
    plt.axis('equal')
    plt.tight_layout()
    plt.show()


# Plot a bar chart of the most popular areas to rent
# Save the rest of neighbourhood data to a csv as this data may be useful
def bar_neighbourhood(df):
    neigh_df = df.filter(['neighbourhood'], axis=1)
    neigh_df = neigh_df.groupby('neighbourhood').size().reset_index(
                                                        name='Count')
    neigh_df = neigh_df.sort_values('Count', ascending=False)
    neigh_df.reset_index(drop=True, inplace=True)
    neigh_df.to_csv(r'RoomsByArea.csv')
    neighs = []
    counts = []
    for index, neigh in neigh_df.iterrows():
        neighs.append(neigh[0][:6] + '.')
        counts.append(neigh[1])
    y_pos = range(len(neighs[:10]))
    plt.bar(neighs[:11], counts[:11], align='center', alpha=1)
    plt.title('10 Most Popular Neighbourhoods for Rent')
    plt.ylabel('Rooms Rented')
    plt.xticks(y_pos, neighs[:10], rotation='vertical')
    plt.show()


# Plot the average cost of every area in a bar chart
# Save data to csv as it may be useful
def price_area(df):
    area_price = df[['neighbourhood_group', 'price']]
    cols = {'neighbourhood_group': 'Area', 'price': 'Average Price'}
    aggregation = {'price': 'mean'}
    area_price = area_price.groupby(['neighbourhood_group'],
                                    as_index=False).agg(
                                    aggregation).rename(columns=cols)
    ax = area_price.plot.bar(x='Area', y='Average Price', rot=0)
    plt.show()
    area_price.to_csv(r'AverageAreaPrice.csv')


# Plot a heat map to show attribute correlation
def plot_corr(df, size=10):
    corr = df.corr()
    fig, ax = plt.subplots(figsize=(size, size))
    ax.matshow(corr)
    plt.xticks(range(len(corr.columns)), corr.columns, rotation='vertical')
    plt.yticks(range(len(corr.columns)), corr.columns)
    sns.heatmap(corr,
                xticklabels=corr.columns,
                yticklabels=corr.columns)
    plt.show()

df = create_df()
plot_coords(df)
pie_rooms(df)
pie_area(df)
bar_neighbourhood(df)
price_area(df)
plot_corr(df)
