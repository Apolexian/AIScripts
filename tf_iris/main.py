# Following the tensorflow.org tutorial
import os

import tensorflow as tf

# Download the data set
url = "https://storage.googleapis.com/download.tensorflow.org/data/iris_training.csv"
data_set = tf.keras.utils.get_file(fname=os.path.basename(url),
                                   origin=url)
# separate column names into descriptions and labels
columns = ['sepal_length', 'sepal_width', 'petal_length', 'petal_width', 'species']
description_fields = columns[:-1]
label_fields = columns[-1]
# define classes for types of iris flowers
classes = ['Iris setosa', 'Iris versicolor', 'Iris virginica']
batch_size = 32
# parse data into suitable format
train_set = tf.data.experimental.make_csv_dataset(data_set, batch_size,
                                                  column_names=columns,
                                                  label_name=label_fields,
                                                  num_epochs=1)


def features_vector(descriptions, labels):
    pack = tf.stack(list(descriptions.values()), axis=1)
    return descriptions, labels


train_set = train_set.map(features_vector)
features, labels = next(iter(train_set))

model = tf.keras.Sequential([
    tf.keras.layers.Dense(10, activation=tf.nn.relu, input_shape=(4,)),
    tf.keras.layers.Dense(10, activation=tf.nn.relu),
    tf.keras.layers.Dense(3)
])

predictions = model(features)
loss_obj = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)


def loss(model, x, y):
    y_ = model(x)
    return loss_obj(y_true=y, y_pred=y_)


l = loss(model, features, labels)


def grad(model, inputs, targets):
    with tf.GradientTape() as tape:
        loss_value = loss(model, inputs, targets)
    return loss_value, tape.gradient(loss_value, model.trainable_variables)


opt = tf.keras.optimizers.Adam(learning_rate=0.01)
loss_value, grads = grad(model, features, labels)
opt.apply_gradients(zip(grads, model.trainable_variables))

train_loss_results = []
train_accuracy_results = []

num_epochs = 200

for epoch in range(num_epochs):
    epoch_loss_avg = tf.keras.metrics.Mean()
    epoch_accuracy = tf.keras.metrics.SparseCategoricalAccuracy()

    # Training loop - using batches of 32
    for x, y in train_set:
        loss_value, grads = grad(model, x, y)
        opt.apply_gradients(zip(grads, model.trainable_variables))

        # Track progress
        epoch_loss_avg(loss_value)
        epoch_accuracy(y, model(x))

    train_loss_results.append(epoch_loss_avg.result())
    train_accuracy_results.append(epoch_accuracy.result())

    if epoch % 50 == 0:
        print("Epoch {:03d}: Loss: {:.3f}, Accuracy: {:.3%}".format(epoch,
                                                                    epoch_loss_avg.result(),
                                                                    epoch_accuracy.result()))

test_url = "https://storage.googleapis.com/download.tensorflow.org/data/iris_test.csv"

test_fp = tf.keras.utils.get_file(fname=os.path.basename(test_url),
                                  origin=test_url)

test_dataset = tf.data.experimental.make_csv_dataset(
    test_fp,
    batch_size,
    column_names=columns,
    label_name='species',
    num_epochs=1,
    shuffle=False)

test_set = test_dataset.map(features_vector)

test_accuracy = tf.keras.metrics.Accuracy()

for (x, y) in test_dataset:
    logits = model(x)
    prediction = tf.argmax(logits, axis=1, output_type=tf.int32)
    test_accuracy(prediction, y)

print("Test set accuracy: {:.3%}".format(test_accuracy.result()))
