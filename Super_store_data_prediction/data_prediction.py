# Data prediction beginner project
# This project was made as a way to learn the basics of data prediction
# This project is purely for self educational purposes
# The data set used was the free Superstore.xls
# Project August 2019


# Import needed modules
import warnings
import pandas as pd
import statsmodels.api as sm
import matplotlib.pyplot as plt
import itertools
from operator import itemgetter
import numpy as np

warnings.filterwarnings("ignore")

# The columns list will be used to drop columns that are not needed
# categories list is list of sales categories in data set
store_categories = ['Furniture', 'Office Supplies', 'Technology']
cols = ['Row ID', 'Order ID', 'Ship Date', 'Ship Mode', 'Customer ID',
        'Customer Name', 'Segment', 'Country', 'City',
        'State', 'Postal Code', 'Region', 'Product ID', 'Category',
        'Sub-Category', 'Product Name', 'Quantity', 'Discount', 'Profit']
# Read data and create data set
data_set = pd.read_excel("Sample_Data.xls")


# Function used to prepare the data in needed form
# The category is singled out, not needed columns are dropped, data is sorted and grouped
# Average daily sales per month are returned
def prep_data(dset, category):
    data = dset.loc[data_set['Category'] == category]
    data.drop(cols, axis=1, inplace=True)
    data = data.sort_values('Order Date')
    data.isnull().sum()
    data = data.groupby('Order Date')['Sales'].sum().reset_index()
    data = data.set_index('Order Date')
    return data['Sales'].resample('MS').mean()


# Plot sales of each category
def plot_data(categories):
    for cat in categories:
        y = prep_data(data_set, cat)
        y.plot(figsize=(15, 6))
    plt.show()


# Plot the time series decomposition for every category
def decomposition(categories):
    decomp = []
    for cat in categories:
        y = prep_data(data_set, cat)
        decompos = sm.tsa.seasonal_decompose(y, model='additive')
        decomp.append(decompos)

    # Function to plot axes, in order to get all decompositions on one figure
    def plot_decomp(res, axies):
        res.observed.plot(ax=axies[0], legend=False)
        axies[0].set_ylabel('Observed')
        res.trend.plot(ax=axies[1], legend=False)
        axies[1].set_ylabel('Trend')
        res.seasonal.plot(ax=axies[2], legend=False)
        axies[2].set_ylabel('Seasonal')
        res.resid.plot(ax=axies[3], legend=False)
        axies[3].set_ylabel('Residual')

    fig, axes = plt.subplots(ncols=3, nrows=4, sharex=True, figsize=(12, 5))
    for i in range(0, len(decomp)):
        plot_decomp(decomp[i], axes[:, i])
    plt.tight_layout()
    plt.show()


# Forecast uses ARIMA modeling to find the best AIC values
# Fit these values to the model, produce a forecast
# This forecast is plotted against observed data
# And RMS error is calculated
def forecast(categories):
    def model_fit(categories):
        # Define all pdq combos and use grid search to find the best AIC
        p = d = q = range(0, 2)
        pdq = list(itertools.product(p, d, q))
        seasonal_pdq = [(x[0], x[1], x[2], 12) for x in list(itertools.product(p, d, q))]
        results_dic = {}
        for cat in categories:
            results_dic[cat] = {}
            y = prep_data(data_set, cat)
            for param in pdq:
                for seasonal_param in seasonal_pdq:
                    try:
                        mod = sm.tsa.statespace.SARIMAX(y, order=param,
                                                        seasonal_order=seasonal_param,
                                                        enforce_stationary=False,
                                                        enforce_invertibility=False)
                        results = mod.fit(disp=-1)
                        results_dic[cat][(param, seasonal_param)] = results.aic
                    except:
                        continue
        # Auto find the best AIC and store in dictionary, for each category of sale
        min_aic = {}
        for k, v in results_dic.items():
            if isinstance(v, dict):
                min_aic[k] = min(v.items(), key=itemgetter(1))
        return min_aic[k]
    # Produce the forecast, plot it against observations and introduce a confidence interval
    for cat in categories:
        y = prep_data(data_set, cat)
        mod = sm.tsa.statespace.SARIMAX(y,
                                        order=model_fit(categories)[0][0],
                                        seasonal_order=model_fit(categories)[0][1],
                                        enforce_stationarity=False,
                                        enforce_invertibility=False)
        results = mod.fit(disp=-1)
        pred = results.get_prediction(start=pd.to_datetime('2017-01-01'), dynamic=False)
        pred_ci = pred.conf_int()
        ax = y['2014':].plot(label='Observation')
        pred.predicted_mean.plot(ax=ax, label='Forecast', alpha=.5, figsize=(10, 5))
        ax.fill_between(pred_ci.index,
                        pred_ci.iloc[:, 0],
                        pred_ci.iloc[:, 1], color='k', alpha=.2)
        ax.set_xlabel('Date')
        ax.set_ylabel('Sales')
        plt.legend()
        plt.show()
        y_forecast = pred.predicted_mean
        y_known = y['2017-01-01':]
        # Calculate and show the RMS error of forecast
        mse = ((y_forecast - y_known) ** 2).mean()
        print(cat + ' ' + 'The RMS error is {}'.format(round(np.sqrt(mse), 2)))


forecast(store_categories) 



