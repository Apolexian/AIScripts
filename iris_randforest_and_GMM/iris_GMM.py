import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.mixture import GaussianMixture as GMM
from sklearn import preprocessing

# Data goes through preprocessing and label encoding is used to map
# String identifiers to float


def data_processor(input_file):
    data = []
    with open(input_file, 'r') as f:
        for line in f:
            line = line.strip().split(',')
            if line[-1] == 'Iris-setosa':
                line[-1] = 0
            elif line[-1] == 'Iris-versicolor':
                line[-1] = 1
            elif line[-1] == 'Iris-virginica':
                line[-1] = 2
            data.append(line)

    return np.array(data, dtype=float)


x, y = data_processor('iris.txt')[:, :-1], data_processor('iris.txt')[:, -1]
x = preprocessing.scale(x)
# split data 80% used for training and 20% for testing
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.20,
                                                    random_state=2)
# calculate number of components in data set using number of classes
num_classes = len(np.unique(y_train))
# build classifier and fit model
classifier = GMM(n_components=num_classes, max_iter=1000,
                 covariance_type='full')
classifier.fit(x_train)

# Make predictions and print accuracy
y_train_pred = classifier.predict(x_train)
accuracy_training = np.mean(y_train_pred.ravel() == y_train.ravel()) * 100
print('Accuracy on training data =', accuracy_training)

y_test_pred = classifier.predict(x_test)
accuracy_testing = np.mean(y_test_pred.ravel() == y_test.ravel()) * 100
print('Accuracy on testing data =', accuracy_testing)
