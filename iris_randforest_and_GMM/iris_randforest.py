import numpy as np
from sklearn.model_selection import cross_validate
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split

# Data goes through preprocessing and label encoding is used to map
# String identifiers to float


def data_processor(input_file):
    data = []
    with open(input_file, 'r') as f:
        for line in f:
            line = line.strip().split(',')
            if line[-1] == 'Iris-setosa':
                line[-1] = 0
            elif line[-1] == 'Iris-versicolor':
                line[-1] = 1
            elif line[-1] == 'Iris-virginica':
                line[-1] = 2
            data.append(line)

    return np.array(data, dtype=float)


# create array of data and array of index
x, y = data_processor('iris.txt')[:, :-1], data_processor('iris.txt')[:, -1]

# create training and test data set, 80% training 20% test
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.20,
                                                    random_state=5)

# params used to create classifier
# number of trees - 150, node depth - 5, random seed
params = {'n_estimators': 150, 'max_depth': 5, 'random_state': 10}
classifier = ExtraTreesClassifier(**params)
classifier.fit(x_train, y_train)
# make prediction
y_test_pred = classifier.predict(x_test)

# output info on model performance
class_names = ['Setosa', 'Versicolor', 'Virginica']
print("Training dataset results\n")
print(classification_report(y_train, classifier.predict(x_train),
                            target_names=class_names))
print("Testing dataset results\n")
print(classification_report(y_test, y_test_pred, target_names=class_names))
