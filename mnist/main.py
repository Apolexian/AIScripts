import tensorflow as tf
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Dropout, Flatten, MaxPooling2D

# Load the data from tensorflow mnist dataser
(xTrain, yTrain), (xTest, yTest) = tf.keras.datasets.mnist.load_data()

# Reshape array to make sure it works with keras API
xTrain = xTrain.reshape(xTrain.shape[0], 28, 28, 1).astype('float32')
xTest = xTest.reshape(xTest.shape[0], 28, 28, 1).astype('float32')
inputShape = (28, 28, 1)
# Normalise images by dividing by maximum RGB value
xTrain /= 255
xTest /= 255

# Define the model using keras API
model = Sequential()
model.add(Conv2D(28, kernel_size=(3, 3), input_shape=inputShape))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Flatten())
model.add(Dense(128, activation=tf.nn.relu))
model.add(Dropout(0.2))
model.add(Dense(10, activation=tf.nn.softmax))

model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
model.fit(x=xTrain, y=yTrain, epochs=10)
model.evaluate(xTest, yTest)
