# The goal is to use silhouette scores to determine
# the best number of clusters for the wine_data
# using KMeans

import numpy as np
from sklearn import preprocessing
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn import metrics

# First process input data and project it into an array
input_file = 'wine_data.txt'
input_data = []
with open(input_file, 'r') as f:
    for line in f:
        line = line.strip().split(',')
        input_data.append(line)
input_data = np.array(input_data, dtype=float)

# As the data spans many values, scale the data
# so that the mean is close to 0 and standard deviation is 1
x = preprocessing.scale(input_data)

# define the range of cluster numbers that will be checked
# and keep track of silhouette scores for each number
cluster_values = np.arange(2, 10)
cluster_scores = []

# for each possible cluster number fit the data using KMeans
# record score and print it
for num_clusters in cluster_values:
    kmeans = KMeans(init='k-means++', n_clusters=num_clusters, n_init=10)
    kmeans.fit(x)
    score = metrics.silhouette_score(x, kmeans.labels_,
                                     metric='euclidean', sample_size=len(x))
    cluster_scores.append(score)
    print("\nWith cluster number =", num_clusters)
    print("Silhouette score =", score)

# visualise results
plt.figure()
plt.bar(cluster_values, cluster_scores, width=0.5, align='center')
plt.title('Silhouette score for cluster number')
plt.show()

# print the optimal number of clusters
num_clusters = np.argmax(cluster_scores) + cluster_values[0]
print('\nOptimal number of clusters =', num_clusters)
