import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow import keras

# Load the data and split it into training and testing data
data_set = keras.datasets.fashion_mnist
(train_images, train_labels), (test_images, test_labels) = data_set.load_data()

# Split images into classes
class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

train_images = train_images / 255.0
test_images = test_images / 255.0

# Setup layers
model = keras.Sequential([
    keras.layers.Flatten(input_shape=(28, 28)),
    keras.layers.Dense(128, activation=tf.nn.relu),
    keras.layers.Dense(10, activation=tf.nn.softmax)
])

# Compile model
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

# Train
model.fit(train_images, train_labels, epochs=8)

# Evaluate the accuracy
test_loss, test_acc = model.evaluate(test_images, test_labels)
print('Accuracy:', test_acc)

# Make the predictions
predictions = model.predict(test_images)


def plot_img(i, predict, evaluation_label, img):
    predict, evaluation_label, img = predict[i], evaluation_label[i], img[i]
    plt.grid(False)
    plt.xticks([])
    plt.yticks([])
    plt.imshow(img, cmap=plt.cm.binary)
    predict_label = np.argmax(predict)
    if predict_label == evaluation_label:
        colour = 'green'
    else:
        colour = 'red'

    plt.xlabel("{} {:2.0f}% ({})".format(class_names[predict_label],
                                         100 * np.max(predict),
                                         class_names[evaluation_label]),
               color=colour)


def plot_val(i, predict, evaluation_label):
    predict, evaluation_label = predict[i], evaluation_label[i]
    plt.grid(False)
    plt.xticks([])
    plt.yticks([])
    plot = plt.bar(range(10), predict, color='blue')
    plt.ylim([0, 1])
    predict_label = np.argmax(predict)
    plot[predict_label].set_color('red')
    plot[evaluation_label].set_color('green')


plt.figure(figsize=(6, 10))
for i in range(10):
    plt.subplot(3, 10, 2 * i + 1)
    plot_img(i, predictions, test_labels, test_images)
    plt.subplot(3, 10, 2 * i + 2)
    plot_val(i, predictions, test_labels)
plt.show()
