## AIScripts

This is a repository that I made for self-educational purposes. The inidvidual projects inside it are a bit small to be their own repos
*However* I do want to have these here for my own future reference and to demonstrate what I have done. 
There are a lot of small projects here, so I will try to keep them all documented via [the wiki](https://github.com/Apolexian/AIScripts/wiki).

