import logging

from numpy import dstack
from pandas import read_csv

# ---------------- Logger configuration ---------------------------------#
logging.basicConfig(filename='app.log',
                    filemode='w',
                    format=' %(name)s - %(asctime)s -  %(levelname)s - %(message)s',
                    level=logging.INFO)


# -------------- Functions Used to Load Data Sets ---------------------- #
def load_csv(file_path):
    try:
        df = read_csv(file_path, header=None, delim_whitespace=True)
        logging.info(f"Loaded file from: {file_path}")
        return df.values
    except OSError:
        logging.exception(f"Exception when loading file {file_path}", exc_info=True)
        return None


def load_multiple(file_names, prefix=''):
    """
    :param file_names: the names of the files that need loading
    :param prefix: prefixes of the files (applied to all)
    :return: the data arrays stacked into three dimensions
    """
    loaded = []
    for name in file_names:
        data = load_csv(prefix + name)
        loaded.append(data)
    loaded = dstack(loaded)
    return loaded


def load_data_set(group, prefix='', nested='/InertialSignals/'):
    """
    :param group: test or train group (the directory name where the sets are)
    :param prefix: the prefix of the file (file path)
    :param nested: the directory where the sets are stored
    :return: train and test data sets as numpy arrays
    """
    abs_path = prefix + group + nested
    file_names = []
    file_names += ['total_acc_x_' +
                   group + '.txt', 'total_acc_y_' +
                   group + '.txt', 'total_acc_z_' +
                   group + '.txt']

    file_names += ['body_acc_x_' +
                   group + '.txt', 'body_acc_y_' +
                   group + '.txt', 'body_acc_z_' +
                   group + '.txt']

    file_names += ['body_gyro_x_' +
                   group + '.txt', 'body_gyro_y_' +
                   group + '.txt', 'body_gyro_z_' +
                   group + '.txt']

    X = load_multiple(file_names, abs_path)
    y = load_csv(prefix + group + '/y_' + group + '.txt')

    return X, y


# ------------------------ Loading Data sets ----------------------#
train_X, train_y = load_data_set('train', 'data/')
logging.info(f"Train_X shape :{train_X.shape}")
logging.info(f"train_y shape: {train_y.shape}")

test_X, test_y = load_data_set('test', 'data/')
logging.info(f"Train_X shape :{test_X.shape}")
logging.info(f"train_y shape: {test_y.shape}")
