import h5py
import numpy as np


# X will contain the actual data of image while Y is index to indicate cat or dog
# Label vectors and process data
# standardize by dividing by max number of pxl
train_data = h5py.File("data_set/train_data.h5")
x_train = np.array(train_data["train_set_x"])
x_train = x_train.reshape(209, -1).T
x_train = x_train / 255
y_train = np.array(train_data["train_set_y"])
y_train = y_train.reshape(-1, 209)

test_data = h5py.File("data_set/test_data.h5")
x_test = np.array(test_data["test_set_x"])
x_test = x_test.reshape(50, -1).T
x_test = x_test / 255
y_test = np.array(test_data["test_set_y"])
y_test = y_test.reshape(-1, 50)


# Activation function sigmoid
def sigmoid(z):
    a = 1/(1+np.exp(-z))
    return a


# Set w to random from normal distribution and bias to 0
def init_params(dims):
    w = np.random.randn(dims, 1)*0.01
    b = 0
    return w, b


# Function to find optimal weight and bias params
def opt(w, b, x, y, lr):
    def propagate():
        m = x.shape[1]
        z = np.dot(w.T, x) + b
        a = sigmoid(z)
        cost = (-np.sum(y * np.log(a) + (1 - y) * np.log(1 - a))) / m
        dw = (np.dot(x, (a - y).T)) / m
        db = np.average(a - y)
        return dw, db, cost

    for i in range(10000):
        dw, db, cost = propagate()
        w = w - lr*dw
        b = b - lr*db
        if i % 1000 == 0:
            print("Cost after iteration %i: %f" % (i, cost))
    return w, b


# Run our model and get cost values
def model(x_train, y_train, lr=0.01):
    w, b = init_params(x_train.shape[0])
    opt(w, b, x_train, y_train, lr)

test = model(x_train, y_train)












